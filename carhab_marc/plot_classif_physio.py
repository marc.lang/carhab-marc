# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt


def custom_ax(ax):
    ax.set_facecolor('ivory')
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.tick_params(axis='x', colors='darkslategrey', labelsize=10)
    ax.tick_params(axis='y', colors='darkslategrey', labelsize=10)
    ax.yaxis.grid(which='major', color='darkgoldenrod', linestyle='--',
                  linewidth=0.75, zorder=1)
    plt.minorticks_on()
    ax.yaxis.grid(which='minor', color='darkgoldenrod', linestyle=':',
                  linewidth=0.5, zorder=1)

    return ax

def load_classif(data_path):
    df = pd.read_csv(data_path, header=[0,1], index_col=[0, 1, 2])
    df.index.names = ["Nb classes", "Algo", "Indice"]
    df.columns.names = ["POC", "Zone"]

    return df

def load_sample(data_path):
    df_samp = pd.read_csv(data_path, header=[0,1], index_col=0)
    df_samp.index.names = ["Strata"]
    df_samp.columns.names = ["POC", "Zone"]

    return df_samp

# ------------------------------------------------------------------ Load data

data_path = ('/DATA/CARHAB/02_Result/physio_elementaire/classifs.csv')
df = load_classif(data_path)

data_path = ('/DATA/CARHAB/02_Result/physio_elementaire/sample.csv')
df_samp = load_sample(data_path)
idx = pd.IndexSlice

#---------------------------------------------------- Apport de la classe ombre

# selection
sel = df.loc[idx[['4','5'], 'RF', ['Kappa', 'OA']],
             [('Scarpe Escault', '1'), ('Scarpe Escault', '2'), ('Avesnois', '1')]]

sel = df.loc[idx[['4','5'], 'RF', ['SN', 'H', 'LB', 'LH', 'O']],
             [('Scarpe Escault', '1'), ('Scarpe Escault', '2'), ('Avesnois', '1')]]

sel = sel.reset_index(level='Algo').drop(columns='Algo')
sel = sel.unstack(level=['Indice']).stack(level=['POC','Zone']).unstack(level='Nb classes')

color = ['lightblue', 'royalblue', 'lightgreen', 'forestgreen']
out_plotname = ('/DATA/CARHAB/02_Result/physio_elementaire/ombre_kappaOA.png')


color = ['lightblue', 'royalblue', 'lightgreen', 'forestgreen', 'lightpink',
         'darkred', 'khaki', 'peru', 'lightgrey', 'black']
order = ['SN', 'H', 'LB', 'LH', 'O']
sel = sel.reindex(columns=order, level='Indice')
out_plotname = ('/DATA/CARHAB/02_Result/physio_elementaire/ombre_classes.png')

# plot
fig, ax = plt.subplots(figsize=(10,7))
ax = sel.plot(kind='bar', color=color, ax=ax, width=0.85, legend=False)
ax.set_xticklabels(['AV - 1', 'SE - 1', 'SE - 2'])
ax = custom_ax(ax)
ax.set_title('Influence de la classe ombre')
ax.set_xlabel('POC - Zone')
plt.savefig(out_plotname, transparent=True, bbox_inches='tight')DATA

#---------------------------------------------------- Apport du regroupement
# 2 classe

sel = df.loc[idx[['2 from 4','2'], 'RF', ['Kappa', 'OA']], idx[['Scarpe Escault', 'Avesnois'], :]]
out_plotname = ('/DATA/CARHAB/02_Result/physio_elementaire/rgpt_kappaOA.png')

sel = df.loc[idx[['2 from 4','2'], 'RF', ['Strate Basse', 'Strate Haute']], idx[['Scarpe Escault', 'Avesnois'], :]]
out_plotname = ('/DATA/CARHAB/02_Result/physio_elementaire/rgpt_strate.png')

color = ['lightblue', 'royalblue', 'lightgreen', 'forestgreen']
tick_label = ['AV - 1', 'AV - 2', 'SE - 1', 'SE - 2']


# 3 classes

sel = df.loc[idx[['3 from 5','3'], 'RF', ['Kappa', 'OA']], idx[['Scarpe Escault', 'Avesnois'], :]]
out_plotname = ('/DATA/CARHAB/02_Result/physio_elementaire/rgpt_kappaOA_3cla.png')
color = ['lightblue', 'royalblue', 'lightgreen', 'forestgreen']

sel = df.loc[idx[['3 from 5','3'], 'RF', ['Strate Basse', 'Strate Haute', 'O']], idx[['Scarpe Escault', 'Avesnois'], :]]
out_plotname = ('/DATA/CARHAB/02_Result/physio_elementaire/rgpt_strate_3cla.png')
color = ['lightblue', 'royalblue', 'lightgreen', 'forestgreen', 'lightpink', 'darkred']
tick_label = ['AV - 1', 'SE - 1', 'SE - 2']

# plot

sel = sel.reset_index(level='Algo').drop(columns='Algo')
sel = sel.unstack(level=['Indice']).stack(level=['POC','Zone']).unstack(level='Nb classes')

fig, ax = plt.subplots(figsize=(10,7))
ax = sel.plot(kind='bar', color=color, ax=ax, width=0.85, zorder=2, legend=False)
ax.set_xticklabels(tick_label)
ax = custom_ax(ax)
ax.set_title('Influence du regroupement')
ax.set_xlabel('POC - Zone')
plt.savefig(out_plotname, transparent=True, bbox_inches='tight')

#---------------------------------------------------- Qualité générale

# ----------- Graphiques simples

sel = df.loc[idx['4', 'RF', ['Kappa', 'OA']], idx[:, :]]
out_plotname = ('/DATA/CARHAB/02_Result/physio_elementaire/RF_4cla_kappaOA.png')
title = u'Qualité générale'

sel = df.loc[idx['4', 'RF', ['SN', 'H', 'LB', 'LH']], idx[:, :]]
out_plotname = ('/DATA/CARHAB/02_Result/physio_elementaire/RF_4cla_Fscore.png')
title = u'F-score'

color = ['lightblue', 'royalblue', 'lightgreen', 'forestgreen', 'lightpink', 'darkred']
tick_label = ['AV - 1', 'SE - 1', 'SE - 2']

# ---- plot

# scores
sel = sel.reset_index(level=['Nb classes', 'Algo']).drop(columns=['Nb classes','Algo'])

fig, ax = plt.subplots(figsize=(10,7))
width=0.85
ax = sel.plot(kind='bar', ax=ax, width=width, zorder=2, legend=False)
ax = custom_ax(ax)
ax.set_xlabel(title)

# mean
means = sel.mean(axis=1)
for pos, m in enumerate(means):
    xmin = pos - width / 2
    xmax = pos + width / 2
    ax.hlines(m, xmin, xmax, zorder=3, linestyles='--')

plt.savefig(out_plotname, transparent=False, bbox_inches='tight')

# Croisement avec le nombre d'échantillon

rf_classif = df.loc[idx['4', 'RF', ['SN', 'H', 'LB', 'LH']], idx[:, :]]
rf_classif = rf_classif.reset_index(level=['Nb classes', 'Algo'])
rf_classif = rf_classif.drop(columns=['Nb classes','Algo'])

rf_samp =