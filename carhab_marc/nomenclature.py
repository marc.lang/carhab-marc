# -*- coding: utf-8 -*-

"""
Function to manipulate nomenclature, convert color file

"""

import os
import pandas as pd
import numpy as np


def pallet_from_iota_to_qgis(iota_pallet, ref_nom, out_qgis_palette):
    """
    Convert the color file txt from iota 2 to the format expected by QGIS

    Parameters
    ----------
    iota_pallet : str
        Path name of the iota2 color table text file
    ref_nom : str
        Path name of the nomenclature
    out_qgis_palette : str
        Out path name of the qgis color table text file

    """
    df = pd.read_csv(iota_pallet, sep=' ', header=None)
    df[4] = 255

    df_nom = pd.read_csv(ref_nom, sep=',', header=None)
    df_nom = df_nom.set_index(0)
    df = df.set_index(0)

    df_fus = df.join(df_nom, rsuffix='label')
    df_fus['test'] = df_fus.index.map(str) + ' ' + df_fus['1label']
    df_fus.drop('1label', axis=1, inplace=True)
    df_fus.to_csv(out_qgis_palette, header=False)

def sub_nomenclature_from_ref(ref_nomenclature):
    """
    From the csv file from the INRA webv site, create 4 sub nomenclature file
    and one overall nomenclature file.

    Parameters
    ----------
    ref_nomenclature : str
        Path of the reference nomenclature. Expected format is classic csv file
    """


    sub_nom_pattern = 'Nomenclature_niv{}.csv'
    sub_nom_pattern = os.path.join(
        os.path.join(os.path.split(ref_nomenclature)[0  ], sub_nom_pattern))
    df = pd.read_csv(ref_nomenclature, sep=',')

    #create empty nomclature
    df_all = pd.DataFrame(data=None, index=None, columns=['code', 'label'])

    #create a nomenclature by level
    lvl_index_range = range(0, 8, 2)
    for lvl_index in lvl_index_range :
        # extract the corresponding level in the ref file
        my_lvl = df.iloc[:,[lvl_index, lvl_index + 1]].dropna(thresh=2)
        my_lvl.columns = ['code', 'label']
        my_lvl.iloc[:, 0] = my_lvl.iloc[:, 0].map(int)

        # write it
        out_nomenclature = sub_nom_pattern.format(lvl_index / 2 + 1)
        my_lvl.to_csv(out_nomenclature, header=None, index=None)

        # fill the overall nomenclature with the current level
        df_all = pd.concat((df_all, my_lvl))

    # write the overall nomenclature
    out_nomenclature = sub_nom_pattern.format('all')
    df_all.to_csv(out_nomenclature, header=None, index=None)

def write_nom_for_matrix(ref_nomenclature, lvl, out_nom, list_code=None):
    """

    Parameters
    ----------
    ref_nomenclature : str
        Path of Carhab nomenclature
    lvl : int
        nomenclature level
    out_nom : str
        outfile name
    list_code : list of int
        If indicated, the output nomenclature will contain only indicated
        code (Optional).
    """

    # load ref nomenclature
    df = pd.read_csv(ref_nomenclature, sep=',', encoding='utf-8')
    # get corresponding level
    code_col = 'code n{}'.format(lvl)
    nom_col = 'nom n{}'.format(lvl)
    # on a subset
    if list_code:
        cond = (df[code_col].isin(list_code))
        sel = df.loc[cond, (code_col, nom_col)]
    else:
        sel = df.loc[:, (code_col, nom_col)]
    # format
    formatted = (sel[nom_col].apply(lambda x: x.replace(':', '-'))  + ':' +
                 sel[code_col].astype('str'))
    # write
    formatted.to_csv(out_nom, header=None, index=None,encoding='utf-8')

def get_update_nom_dict(corres_csv):
    """
    Parameters
    ----------
    corres_csv : str
        Path of correspondence file between old and new nomenclature
    Returns
    -------
    dict_update : dict
        keys correspond to the old codes and values to the new codes

    """
    change_df = pd.read_csv(corres_csv, encoding='utf-8')
    change_df.columns
    change_df.drop(columns=['Niveau 4 old', 'Niveau 4 new'], inplace=True)
    return change_df.set_index('Code n4 old').to_dict()['Code n4 new']

def get_change_dict(nomenclature_csv, lvl_sup, lvl_inf):
    """
    Get correspondance of both code and name of lower classes to upper classes

    Parameters
    ----------
    nomenclature_csv : str
        Path of Carhab nomenclature
    lvl_inf : int
        Classification level source
    lvl_sup : int
        Classification level destination

    Returns
    -------
    dict_change : dict
        Contain two dictionnary, one containing the corrsponding code and one
        the corresponding label of classes of upper level.

        {'code n_sup' : {code n_inf : code n_sup},
         'nom n_sup' :  {code n_inf : nom n_sup}}

    """

    df = pd.read_csv(nomenclature_csv, sep=',', encoding='utf-8')
    lvl_index = (lvl_sup - 1) * 2
    my_lvl = df.iloc[:, [lvl_index, lvl_index + 1]].dropna(thresh=2)
    my_lvl.columns = ['code', 'label']
    my_lvl.iloc[:, 0] = my_lvl.iloc[:, 0].map(int)
    my_lvl = my_lvl.set_index(['code'])

    lvl_index2 = (lvl_inf - 1) * 2
    my_lvl2 = df.iloc[:, [lvl_index2, lvl_index2 + 1]].dropna(thresh=2)
    my_lvl2.columns = ['code', 'label']
    my_lvl2.iloc[:, 0] = my_lvl2.iloc[:, 0].map(int)
    div = 10**(lvl_inf - lvl_sup)
    my_lvl2['code_sup'] = (my_lvl2['code'] / div).map(int)
    my_lvl2 = my_lvl2.set_index(['code_sup'])
    my_lvl2 = my_lvl2.join(my_lvl, rsuffix='_sup')
    my_lvl2 = my_lvl2.reset_index()
    my_lvl2 = my_lvl2.set_index(['code', 'label'])
    my_lvl2.index.names= ['code n{}'.format(lvl_inf),
                          'label  n{}'.format(lvl_inf)]
    my_lvl2.columns = ['code n{}'.format(lvl_sup),
                           'label n{}'.format(lvl_sup)]

    return my_lvl2.reset_index(level=1, drop=True).to_dict()


def main():
    ref_nomenclature = ('/DATA/CARHAB/00_Data/nomenclature/NomenclatureRevue.csv')
    sub_nomenclature_from_ref(ref_nomenclature)
    
    iota_pallet = ('/DATA/CARHAB/00_Data/nomenclature/colorFile_n2_new.txt')
    out_qgis_palette = ('/DATA/CARHAB/00_Data/nomenclature/colorFile_n2_qgis.txt')
    ref_nom = ('/DATA/CARHAB/00_Data/nomenclature/Nomenclature_nivall.csv')
    pallet_from_iota_to_qgis(iota_pallet, ref_nom, out_qgis_palette)
