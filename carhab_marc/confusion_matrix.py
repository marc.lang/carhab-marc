# -*- coding: utf-8 -*-

import sys
iota2_path = '/home/marc/iota2/iota2/scripts'
sys.path.append(iota2_path)
import Validation.ResultsUtils as resU
import os
import argparse
import pandas as pd
from copy import deepcopy
from carhab_marc import nomenclature as nom

MERGE_CLASS = [{"src_label": [2110, 2120],
                    "dst_label": 21,
                    "dst_name": "Minéral"},
                   {"src_label": [3111],
                    "dst_label": 31,
                    "dst_name": "Pelouse"},
                   {"src_label": [3210, 3221, 3222],
                    "dst_label": 32,
                    "dst_name": "Prairie et herbacé haut"},
                   {"src_label": [4210],
                    "dst_label": 42,
                    "dst_name": "Arbustif/Fourrées/Fructicées"},
                   {"src_label": [5210],
                    "dst_label": 52,
                    "dst_name": "Forêt IGN"}
                   ]

def get_labels_from_matrix(in_matrix_csv):
    """
    Extract the labels / classes of a confusion matrix

    Parameters
    ----------
    in_matrix_csv : str
        Path of csv matrix file
    Returns
    -------
    list_label : list of int
        The list of the labels / classes
    """
    in_matrix = open(in_matrix_csv)
    # get rows labels
    in_line = in_matrix.next()
    in_line = in_line.split('\n')[0]
    begin, end = in_line.split(':')
    list_label_rows = end.split(',')

    # get columns labels
    in_line = in_matrix.next()
    in_line = in_line.split('\n')[0]
    begin, end = in_line.split(':')
    list_label_columns = end.split(',')

    # merge
    list_label = list_label_rows
    for lab in list_label_columns:
        if lab not in list_label:
            list_label.append(lab)

    in_matrix.close()
    return list_label

def get_merge_class_list(ref_nomenclature, in_matrix_csv):
    """
    List of dict of class to merge to the format exepect by iota2 based on the
    classes presented in a confusion matrix and a reference nomenclature

    Parameters
    ----------
    ref_nomenclature : str
        Path of Carhab nomenclature
    in_matrix_csv : str
        Path of csv matrix file
    Returns
    -------
    merge_class : list of dict
        List of dict of class to merge to the format exepect by iota2 :

            {"src_label": [2110, 2120],
             "dst_label": 21,
             "dst_name": "Minéral"}

    """

    change_dict = nom.get_change_dict(ref_nomenclature, 2, 4)
    list_labels = get_labels_from_matrix(in_matrix_csv)

    temp_merge_class = {}
    merge_class = []
    for code in list_labels:
        code = int(code)
        dst_label = change_dict['code n2'][code]
        if dst_label not in temp_merge_class.keys():
            temp_merge_class[dst_label] = {'src_label': [],
                                           'dst_name': change_dict['label n2'][
                                               code].encode('utf8')}
        temp_merge_class[dst_label]['src_label'].append(code)
    for dst_label, dict_temp in temp_merge_class.iteritems():
        dict_temp['dst_label'] = dst_label
        merge_class.append(dict_temp)
    return merge_class

def change_matrix_label(dict_change, in_matrix_csv, out_matrix_csv):
    """
    Change label(s) of confusion matrix.

    Parameters
    ----------
    dict_change : dict
         keys corresponding to the old labels and values to the new labels
    in_matrix_csv : str
        Path of csv matrix file to update
    out_matrix_csv : str
        Path of output csv matrix file
    """

    in_matrix = open(in_matrix_csv)
    out_matrix = open(out_matrix_csv, 'w')
    for num_line, in_line in enumerate(in_matrix):
        if num_line in [0, 1]:  #  only first row to change
            # get labels
            begin, end = in_line.split(':')
            list_label = end.split(',')
            # update labels
            new_list_label = []
            for label in list_label:
                new_list_label.append((str(dict_change[int(label)])
                                      if (int(label) in dict_change.keys())
                                      else label))
            new_end = ','.join(new_list_label)
            out_line = ':'.join([begin, new_end]) + '\n'
            # write it
            out_matrix.writelines([out_line])
        else:  # copy previous matrix
            out_matrix.writelines([in_line])
    in_matrix.close()
    out_matrix.close()


def merge_class_filter(nomenclature):

    df = pd.read_csv(nomenclature, sep=':', header=None)
    list_class = list(df.iloc[:,1])
    merge_class = [] #deepcopy(MERGE_CLASS)
    for i, changes in enumerate(MERGE_CLASS):
        in_changes_filtered = [in_change for in_change in changes["src_label"]
                               if in_change in list_class]
        if len(in_changes_filtered) > 0:
            merge_class.append(deepcopy(changes))
            merge_class[-1]['src_label'] = in_changes_filtered
    return merge_class

def command_line_arguments():
    parser = argparse.ArgumentParser(
        description=("from a csv file representing a confusion matrix " +
                     "generate the corresponding figure"))
    parser.add_argument("--csv_in", required=True,
                        help=("path to a csv confusion matrix (OTB's" +
                              "ComputeConfusionMatrix output)"))
    parser.add_argument("--out_png", required=True,
                        help=("output path"))
    parser.add_argument('-nom', "--nomenclature_path", required=True,
                        help=("path to the file which describre the" +
                              "nomenclature"))
    parser.add_argument("-c", "--conf_score", default='percentage',
                        help=("'count' / 'percentage'"))
    return parser.parse_args()

def main():
    args = command_line_arguments()
    resU.gen_confusion_matrix_fig(args.csv_in, args.out_png, args.nomenclature_path,
                                  undecidedlabel=None, dpi=300,
                                  write_conf_score=True, grid_conf=True,
                                  conf_score=args.conf_score, threshold=0.1)

if __name__ == "__main__":
    main()
