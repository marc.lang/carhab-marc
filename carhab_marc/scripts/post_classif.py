# -*- coding: utf-8 -*-

from carhab_marc import nomenclature as nom
from ymraster import RasterDataType, Raster
from carhab_marc.tools import mask_and_crop


# -------------- crop to POC

# lvl 2 (from regroup)

raster_name = ('/DATA/CARHAB/02_Result/classif/av/prod_avesnois_sc2_n4/' +
               'Classifications_fusion_ColorIndexed.tif')
mask_file = ('/DATA/CARHAB/00_Data/zones/Zone_PNRAv.shp')
nodata = 0
out_filename = ('/DATA/CARHAB/02_Result/classif/av/prod_avesnois_sc2_n4/' +
               'Classifications_fusion_ColorIndexed_lvl4_crop.tif')
crop = True
mask_and_crop(raster_name, mask_file, nodata, out_filename, crop=crop)

# ------------- change code of classification

# correspondance
change_file = ('/DATA/CARHAB/00_Data/nomenclature/' +
               'NomenclatureActualiséeCorrespondance.csv')
update_dict = nom.get_update_nom_dict(change_file)
classif = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
            'Classifications_fusion_ColorIndexed.tif')
classif = ('/DATA/CARHAB/02_Result/classif/av/prod_avesnois_sc2_n4/' +
            'Classifications_fusion_ColorIndexed_lvl4_crop.tif')
rst = Raster(classif)
idx_band = 1
out_filename = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
                'Classifications_fusion_ColorIndexed_lvl4_newnom.tif')
out_filename = ('/DATA/CARHAB/02_Result/classif/av/prod_avesnois_sc2_n4/' +
                'Classifications_fusion_ColorIndexed_lvl4_crop_newnom.tif')
dtype = RasterDataType(lstr_dtype='uint16')
rst.change_value(update_dict, idx_band, out_filename=out_filename, dtype=dtype)


#  ------------ regroup a classif

# input param
nomenclature_csv = (
        '/DATA/CARHAB/00_Data/nomenclature/NomenclatureRevue.csv')
lvl_inf = 4
lvl_sup = 2
dtype = RasterDataType(lstr_dtype='uint16')
out_filename = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
                'Classifications_fusion_ColorIndexed_lvl4_newnom_regroup2.tif')
out_filename = ('/DATA/CARHAB/02_Result/classif/av/prod_avesnois_sc2_n4/' +
                'Classifications_fusion_ColorIndexed_lvl4_crop_newnom_regroup2.tif')

raster_name = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
                'Classifications_fusion_ColorIndexed_lvl4_newnom.tif')
raster_name = ('/DATA/CARHAB/02_Result/classif/av/prod_avesnois_sc2_n4/' +
               'Classifications_fusion_ColorIndexed_lvl4_crop_newnom.tif')

# get correspondence between lvl2 and lvl4
dict = nom.get_change_dict(nomenclature_csv, lvl_sup, lvl_inf)
dict_change = dict['code n{}'.format(lvl_sup)]
idx_band=1

# regroup
rst = Raster(raster_name)
rst.change_value(dict_change, idx_band, out_filename=out_filename, dtype=dtype)

# set nodata value
rst = Raster(out_filename)
rst.nodata_value = 0


# -------------- crop to POC

# lvl 2 (from regroup)

raster_name = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
                'Classifications_fusion_ColorIndexed_lvl4_newnom_regroup2.tif')
mask_file = ('/DATA/CARHAB/00_Data/zones/Zone_PNRSE.shp')
nodata = 0
out_filename = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
        'Classifications_fusion_ColorIndexed_lvl4_newnom_regroup2_crop.tif')
crop = True
mask_and_crop(raster_name, mask_file, nodata, out_filename, crop=crop)

# lvl 4
raster_name = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
                'Classifications_fusion_ColorIndexed_lvl4_newnom.tif')
mask_file = ('/DATA/CARHAB/00_Data/zones/Zone_PNRSE.shp')
nodata = 0
out_filename = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
        'Classifications_fusion_ColorIndexed_lvl4_newnom_crop.tif')
crop = True
mask_and_crop(raster_name, mask_file, nodata, out_filename, crop=crop)