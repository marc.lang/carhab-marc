# -*- coding: utf-8 -*-

import geopandas
from osgeo import ogr
import fiona
from shapely.geometry import mapping, Polygon
import glob
import pandas as pd
from os import path
import matplotlib.pyplot as plt
idx = pd.IndexSlice
from tabulate import tabulate

from carhab_marc import nomenclature as nom

# initiale et sam
corres_init = {u'Périgord centre': 'echant_physio_1906_24_PERIGORD_CENTRAL.shp',
    u'Double Landais': 'echant_physio_1906_24_DOUBLE_LANDAIS.shp',
    u'Riberacois': 'echant_physio_1906_Dordogne_UEP Riberacois.shp',
    u'Périgord cristallin': 'echant_physio_1906_24_PERIGORD_CRISTALLIN.shp',
    u'Bergeracois': 'echant_physio_1906_24_BERGERACOIS.shp',
    u'Périgord noir': 'echant_physio_1906_24_PERIGORD_NOIR.shp',
    u'Causses': 'echant_physio_1906_24_CAUSSES.shp'}

# après vérification zonage
corres_zones = {u'Périgord centre': 'echant_physio_1906_24_PERIGORD_CENTRAL.shp',
    u'Double Landais': 'echant_physio_1906_24_DOUBLE_LANDAIS.shp',
    u'Riberacois': 'echant_physio_1906_Dordogne_UEP Riberacois.shp',
    u'Périgord cristallin': 'echant_physio_1906_24_PERIGORD_CRISTALLIN_corrige.shp',
    u'Bergeracois': 'echant_physio_1906_24_BERGERACOIS.shp',
    u'Périgord noir': 'echant_physio_1906_24_PERIGORD_NOIR.shp',
    u'Causses': 'echant_physio_1906_24_CAUSSES_corrige.shp'}

# après recodage
corres_new_code = {u'Périgord centre': 'echant_physio_1906_24_PERIGORD_CENTRAL_newcode.shp',
    u'Double Landais': 'echant_physio_1906_24_DOUBLE_LANDAIS_newcode.shp',
    u'Riberacois': 'echant_physio_1906_Dordogne_UEP Riberacois_newcode.shp',
    u'Périgord cristallin': 'echant_physio_1906_24_PERIGORD_CRISTALLIN_corrige_newcode.shp',
    u'Bergeracois': 'echant_physio_1906_24_BERGERACOIS_newcode.shp',
    u'Périgord noir': 'echant_physio_1906_24_PERIGORD_NOIR_newcode.shp',
    u'Causses': 'echant_physio_1906_24_CAUSSES_corrige_newcode.shp'}


data_fold_moi = '/DATA/CARHAB/00_Data/echantillon/DO/'
data_fold_sam = '/DATA/CARHAB/00_Data/echantillon/DO/post_treated/'
code_n4 = 'CODE_N4'
label_n4 = 'NOM_N4'

corres = {u'Avesnois': 'AV/echant_physio_1904_PNRAv_CBNBL_corriges.shp',
          u'Scarpe Escault': 'SE/echant_physio_1904_SE_corriges.shp',}
data_fold = '/DATA/CARHAB/00_Data/echantillon'
code_n4 = 'coden4'
label_n4 = 'PHYSIONIV4'

list_zone = corres.keys()
zones_file = ('/DATA/CARHAB/00_Data/zones/UEP24/stratif_UEP.shp')
zones = geopandas.read_file(zones_file)

# ------------------- test si les échantillons sont bien inclus dans les zones

# --- Trouver des zones problématiques

corres = corres_init
data_fold = data_fold_moi
for zone in list_zone:
    sample_file = path.join(data_fold, corres[zone])
    a_sample = geopandas.read_file(sample_file)
    geom_zone = zones.loc[zones[u'NOM_UNITÉ'] == zone, 'geometry'].iloc[0]
    a_sample['inclusion'] = a_sample.within(geom_zone)
    print(zone)
    print(a_sample['inclusion'].value_counts())

# deux zones à corriger :
# - Causses : 52
# - Bergeracois : 1

# --- Trouver où devrait se situer ces échantillons

# load
zone = 'Causses'
sample_file = path.join(data_fold, corres[zone])
a_sample = geopandas.read_file(sample_file)
geom_zone = zones.loc[zones[u'NOM_UNITÉ'] == zone, 'geometry'].iloc[0]

# échantillons non inclues
a_sample['inclusion'] = a_sample.within(geom_zone)
mis_sample = a_sample[a_sample['inclusion'] == False]
mis_sample['location'] = 'inconnu'

# trouver la zone où ils devraient être
for zone in list_zone:
    if zone == 'Causses':
        continue
    geom_zone = zones.loc[zones[u'NOM_UNITÉ'] == zone, 'geometry'].iloc[0]
    mis_sample['location'][mis_sample.within(geom_zone)] = zone

# Une seule zone (ouf) Le Périgord Cristallin. Deux échantillons ne sont inclus
# dans aucune zones : le 82 et le 207. Ils intersectent deux zones en fait.
# le 82 est à motié sur le Causses et le Périgord Noir, tu du coup on le laisse
# dans les causses. L'autre est est majoritairement dans le Périgord cristallin
# dans on le met là.

# on nétoie les champs
mis_sample = mis_sample.drop(index=82)
mis_sample.loc[207, 'location'] = 'Périgord cristallin'
mis_sample = mis_sample.drop(columns=['id', 'location', 'inclusion'])

# ---  les ajouter au Périgord cirstallin
zone = u'Périgord cristallin'
sample_file = path.join(data_fold, corres[zone])
a_sample = geopandas.read_file(sample_file)

# save la source des écahantillons dans de nouveaux champs
mis_sample['id_origi'] = mis_sample.index
mis_sample['zone_origi'] = 'Causses'
a_sample['id_origi'] = a_sample.index
a_sample['zone_origi'] = u'Périgord cristallin'

new_peri = pd.concat([a_sample, mis_sample], ignore_index=True)
name_file = 'echant_physio_1906_24_PERIGORD_CRISTALLIN_corrige.shp'
new_peri.to_file(path.join(data_fold, name_file), encoding='utf-8')

# --- les enlever des causses
zone = u'Causses'
sample_file = path.join(data_fold, corres[zone])
a_sample = geopandas.read_file(sample_file)

a_sample.drop(index= mis_sample.index, inplace=True)
name_file = 'echant_physio_1906_24_CAUSSES_corrige.shp'
a_sample.to_file(path.join(data_fold, name_file), encoding='utf-8')


# ------------------- Nouveaux codes pour mes échantillons
corres = corres_zones
data_fold = data_fold_moi

change_file = ('/DATA/CARHAB/00_Data/nomenclature/' +
               'NomenclatureActualiséeCorrespondance.csv')
change_df = pd.read_csv(change_file, encoding='utf-8')


for zone, name_file in corres.iteritems():
    print zone
    sample_file = path.join(data_fold, name_file)
    a_sample = geopandas.read_file(sample_file, encoding='utf-8')
    for idx, row in change_df.iterrows():
        cond = (a_sample['CODE_N4'].astype(int) == row['Code n4 old'])
        a_sample.loc[cond, 'CODE_N4'] = str(row['Code n4 new'])
        a_sample.loc[cond, 'NOM_N4'] = row['Niveau 4 new']
    suffix = '_newcode'
    base, ext = path.splitext(name_file)
    name_file =  base + suffix + ext
    a_sample.to_file(path.join(data_fold, name_file), encoding='utf-8')

# ------------------- Pb d'encodage pour Perigord cristallin sam + Nom des labels qui ne sont pas hamronisés non plus
corres =corres_init
data_fold = data_fold_sam
change_file = ('/DATA/CARHAB/00_Data/nomenclature/' +
               'NomenclatureActualiséeCorrespondance.csv')
change_df = pd.read_csv(change_file, encoding='utf-8')

for zone, name_file in corres.iteritems():
    print(zone)
    sample_file = path.join(data_fold, name_file)
    a_sample = geopandas.read_file(sample_file, encoding='utf-8')
    for idx, row in change_df.iterrows():
        cond = (a_sample['CODE_N4'].astype(int) == row['Code n4 new'])
        a_sample.loc[cond, 'NOM_N4'] = row['Niveau 4 new']
    a_sample.to_file(sample_file, encoding='utf-8')

# ------------------- Enlever certaines classes

corres = corres_new_code
data_fold = data_fold_moi

list_to_delete = [6010, 6020, 5210, 5220]

for zone, name_file in corres.iteritems():
    print(zone)
    sample_file = path.join(data_fold, name_file)
    a_sample = geopandas.read_file(sample_file, encoding='utf-8')
    for delete_code in list_to_delete :
        cond = (a_sample['CODE_N4'].astype(int) == delete_code)
        a_sample.drop(a_sample.loc[cond].index, inplace=True, errors='ignore')
    suffix = '_v2'
    base, ext = path.splitext(name_file)
    name_file = base + suffix + ext
    sample_file = path.join(data_fold, name_file)
    a_sample.to_file(sample_file, encoding='utf-8')

# ----------------- Rajouter le niveau 2
corres = corres_new_code
data_fold = data_fold_moi

nom_csv_path = ('/DATA/CARHAB/00_Data/nomenclature/NomenclatureRevue.csv')
dict_nom = nom.get_change_dict(nom_csv_path, 2, 4)
dict_code = dict_nom['code n2']
dict_label = dict_nom['label n2']

for zone, basename_file in corres.iteritems():
    # load
    suffix = '_v2'
    base, ext = path.splitext(basename_file)
    name_file = base + suffix + ext
    sample_file = path.join(data_fold, name_file)
    a_sample = geopandas.read_file(sample_file, encoding='utf-8')

    # create lvl2 columns
    list_code = a_sample[code_n4].astype('int').unique()
    for code in list_code:
        cond = (a_sample['CODE_N4'].astype(int) == code)
        a_sample.loc[cond, 'CODE_N2'] = str(dict_code[code])
        a_sample.loc[cond, 'NOM_N2'] = dict_label[code]

    # save
    suffix = '_v3'
    base, ext = path.splitext(basename_file)
    name_file = base + suffix + ext
    sample_file = path.join(data_fold, name_file)
    a_sample.to_file(sample_file, encoding='utf-8')

# ------------------- Inventaire des échantillons par classes

corres =corres_init
data_fold = data_fold_sam

corres =corres_new_code
data_fold = data_fold_moi

list_count = []
for zone, name_file in corres.iteritems():
    sample_file = path.join(data_fold, name_file)
    a_sample = geopandas.read_file(sample_file, encoding='utf-8')
    count = a_sample.groupby(by=[code_n4, label_n4]).count()['geometry']
    print(zone)
    print(count)
    list_count.append(count.to_frame(name=zone))

# mes traitements
synthese = pd.concat(list_count, join='outer', axis=1)

# ceux de samuel
synthese2 = pd.concat(list_count, join='outer', axis=1)
idx1 = synthese.index
idx2 = synthese2.index
dif = idx1.difference(idx2)

synthese.loc[dif]

synthese.to_csv('/DATA/CARHAB/00_Data/nomenclature/dordogne.csv',  encoding='utf-8')
synthese2.to_csv('/DATA/CARHAB/00_Data/nomenclature/dordogne_sam.csv',  encoding='utf-8')

print(tabulate(synthese2.reset_index(), tablefmt='pipe', headers='keys',
               showindex=False))

print(tabulate(synthese.loc[dif].reset_index(), tablefmt='pipe', headers='keys',
               showindex=False))

# ------------------------------------------------------ test de l'aire minimal

# -- test de manière générale

corres =corres_init
data_fold = data_fold_sam

aire_mins = [n_pix * 100 for n_pix in range(0,11)]
columns = ['aire > {}'.format(n_pix) for n_pix in range(0,11)]
synthese = pd.DataFrame(index=corres.keys(), columns=columns)

for zone, name_file in corres.iteritems():
    for aire_min in aire_mins:
        sample_file = path.join(data_fold, name_file)
        a_sample = geopandas.read_file(sample_file)
        n_pix = aire_min / 100
        label_col = 'aire > {}'.format(n_pix)
        a_sample[label_col] = a_sample.area > aire_min
        n_ok = a_sample[label_col].value_counts().loc[True]
        synthese.loc[zone, label_col] = n_ok
        #print(zone)
        #print(a_sample[label_col].value_counts())


ax = synthese.T.plot()
ax.set_xlabel('Area in pixel S2')
ax.set_ylabel('Number of samples ')

ax.set_facecolor('ivory')
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.spines["bottom"].set_visible(False)
ax.spines["left"].set_visible(False)
ax.tick_params(axis='x', colors='darkslategrey', labelsize=10)
ax.tick_params(axis='y', colors='darkslategrey', labelsize=10)
ax.yaxis.grid(which='major', color='darkgoldenrod', linestyle='--',
              linewidth=0.75, zorder=1)
plt.minorticks_on()
ax.yaxis.grid(which='minor', color='darkgoldenrod', linestyle=':',
              linewidth=0.5, zorder=1)
loc = ax.get_xticks()
plt.xticks(range(0,11,2), range(0,12,2))
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

out_plot_name = ('/DATA/CARHAB/02_Result/sample_analysis/figure/' +
                 'number_of_sample.png')
plt.savefig(out_plot_name, bbox_inches='tight', facecolor="None")


# -- test par nombre de classe

corres =corres_init
data_fold = data_fold_sam

th_n_samp = 5 # une classe est considérée si elle contient plus de th_n_smap
plot_a_zone = True

aire_mins = [n_pix * 100 for n_pix in range(0,11)]
columns = ['aire > {}'.format(n_pix) for n_pix in range(0,11)]

n_class_ok = pd.DataFrame(columns=corres.keys(), index=columns)
for zone in corres.keys():
    synthese = pd.DataFrame(columns=columns)
    #zone = 'Causses'
    sample_file = path.join(data_fold, corres[zone])
    a_sample = geopandas.read_file(sample_file)
    for aire_min in aire_mins:
        n_pix = aire_min / 100
        label_col = 'aire > {}'.format(n_pix)
        a_sample[label_col] = a_sample.area >  aire_min
        sample_ok = a_sample[a_sample[label_col] == True]
        synthese[label_col] = sample_ok.groupby(by='CODE_N4').count()['NOM_N4']
    n_class_ok[zone] = synthese.shape[0] - synthese.isna().sum()

    if plot_a_zone:
        ax = synthese.T.plot(zorder=1)
        ax.set_title(zone)
        ax.set_xlabel('Area in pixel S2')
        ax.set_ylabel('Number of samples')
        plt.xticks(range(0,12,2), range(0,12,2))
        ax.hlines(th_n_samp, 0, 10, linestyle='--', linewidth=1, zorder=2,
                  color='black')
        ax.fill_between(range(0,11),th_n_samp, 0, color='saddlebrown', alpha=0.3)

        ax.set_facecolor('ivory')
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.spines["bottom"].set_visible(False)
        ax.spines["left"].set_visible(False)
        ax.tick_params(axis='x', colors='darkslategrey', labelsize=10)
        ax.tick_params(axis='y', colors='darkslategrey', labelsize=10)
        ax.yaxis.grid(which='major', color='darkgoldenrod', linestyle='--',
                      linewidth=0.75, zorder=1)
        plt.minorticks_on()
        ax.yaxis.grid(which='minor', color='darkgoldenrod', linestyle=':',
                      linewidth=0.5, zorder=1)

        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        out_plot_name = (u'/DATA/CARHAB/02_Result/sample_analysis/figure/' +
                         u'number_of_sample_{}.png'.format(zone))
        plt.savefig(out_plot_name, bbox_inches='tight', facecolor="None")

n_class_ok[zone] = synthese.shape[0] - synthese.isna().sum()

plt.figure()
ax = n_class_ok.plot()
ax.set_xlabel('Number of S2 pixel')
ax.set_ylabel('Number of classes (with n_samp > 5)')
plt.xticks(range(0,12,2), range(0,12,2))

ax.set_facecolor('ivory')
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.spines["bottom"].set_visible(False)
ax.spines["left"].set_visible(False)
ax.tick_params(axis='x', colors='darkslategrey', labelsize=10)
ax.tick_params(axis='y', colors='darkslategrey', labelsize=10)
ax.yaxis.grid(which='major', color='darkgoldenrod', linestyle='--',
              linewidth=0.75, zorder=1)
plt.minorticks_on()
ax.yaxis.grid(which='minor', color='darkgoldenrod', linestyle=':',
              linewidth=0.5, zorder=1)

# Shrink current axis by 20%
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
# Put a legend to the right of the current axis
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
out_plot_name = ('/DATA/CARHAB/02_Result/sample_analysis/figure/' +
                 'number_of_classe.png')
plt.savefig(out_plot_name, bbox_inches='tight', facecolor="None")

# ----------------------------------------------------- test de la nomenclature
corres = {u'Périgord centre': 'echant_physio_1906_24_PERIGORD_CENTRAL.shp',
    u'Double Landais': 'echant_physio_1906_24_DOUBLE_LANDAIS.shp',
    u'Riberacois': 'echant_physio_1906_Dordogne_UEP Riberacois.shp',
    u'Périgord cristallin': 'echant_physio_1906_24_PERIGORD_CRISTALLIN_corrige.shp',
    u'Bergeracois': 'echant_physio_1906_24_BERGERACOIS.shp',
    u'Périgord noir': 'echant_physio_1906_24_PERIGORD_NOIR.shp',
    u'Causses': 'echant_physio_1906_24_CAUSSES_corrige.shp'}


ref_nomenclature = ('/DATA/CARHAB/00_Data/nomenclature/Nomenclature_niv4.csv')
nom = pd.read_csv(ref_nomenclature)
nom.columns = ['code', 'label']

zone = 'Causses'
sample_file = path.join(data_fold, corres[zone])
a_sample = geopandas.read_file(sample_file)

pb = a_sample['CODE_N4'].apply(lambda x: float(x) not in nom['code'].values)
a_sample.loc[pb]