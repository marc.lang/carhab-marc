# -*- coding: utf-8 -*-

import sys
import os
iota2_path = '/home/marc/iota2/iota2/scripts'
sys.path.append(iota2_path)
import Validation.ResultsUtils as resU
from carhab_marc import confusion_matrix as cf
from carhab_marc import nomenclature as nom

# ---- gen confusion matrix (from old nomenclature to new one)
change_file = ('/DATA/CARHAB/00_Data/nomenclature/' +
               'NomenclatureActualiséeCorrespondance.csv')
dict_change = nom.get_update_nom_dict(change_file)
in_csv = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
          'merge_final_classifications/confusion_mat_maj_vote.csv')
out_csv = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
          'merge_final_classifications/confusion_mat_maj_vote_newnom.csv')
cf.change_matrix_label(dict_change, in_csv, out_csv)

# --- gen nomenclature
# nomenclature expected for the computation of confusion matrix plot must
# contains only the classes presented in the confusion matrix. This part
# gererated one.

# parameters
ref_nomenclature = ('/DATA/CARHAB/00_Data/nomenclature/NomenclatureRevue.csv')
out_nomenclature =  ('/DATA/CARHAB/00_Data/nomenclature/Nomenclature_test.txt')
csv_in = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/' +
          'merge_final_classifications/confusion_mat_maj_vote_newnom.csv')
lvl = 4

list_labels = cf.get_labels_from_matrix(csv_in)
nom.write_nom_for_matrix(ref_nomenclature, lvl, out_nomenclature,
                         list_code=list_labels)

# --- gen matrix plot

# comon part
folder = ('/DATA/CARHAB/02_Result/classif/se/prod_scarpeescaut_sc2_n4/'
          + 'merge_final_classifications')
nomenclature = ('/DATA/CARHAB/00_Data/nomenclature/Nomenclature_test.txt')

def j(name): return os.path.join(folder, name)

# - for lvl4
label_type = 'label'
csv_in = j('confusion_mat_maj_vote_newnom.csv')
out_pngs = [j('lv4_per_newnom.png'),
            j('lv4_sci_newnom.png')]
list_conf_score = ['percentage', 'count_sci']

for out_png, conf_score in zip(out_pngs, list_conf_score):
    resU.gen_confusion_matrix_fig(csv_in, out_png,
                                  nomenclature,
                                  undecidedlabel=None, dpi=300,
                                  write_conf_score=True, grid_conf=True,
                                  conf_score=conf_score, threshold=0.1,
                                  label_type=label_type)

# - for lvl2 from lvl4
label_type = 'label'
csv_in = j('confusion_mat_maj_vote_newnom.csv')
out_pngs = [j('lv2_per_newnom_regroup.png'),
            j('lv2_sci_newnom_regroup.png')]
list_conf_score = ['percentage', 'count_sci']

# get merge class dict
ref_nomenclature = ('/DATA/CARHAB/00_Data/nomenclature/NomenclatureRevue.csv')
merge_class = cf.get_merge_class_list(ref_nomenclature, csv_in)

# plot
for out_png, conf_score in zip(out_pngs, list_conf_score):
    resU.gen_confusion_matrix_fig(csv_in, out_png,
                                  nomenclature,
                                  undecidedlabel=None, dpi=300,
                                  write_conf_score=True, grid_conf=True,
                                  conf_score=conf_score, threshold=0.1,
                                  label_type=label_type,
                                  merge_class=merge_class)