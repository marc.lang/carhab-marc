# -*- coding: utf-8 -*-

import geopandas
from osgeo import ogr
import fiona
from shapely.geometry import mapping, Polygon
import os
import glob
from os import path
from pprint import pprint
import pandas as pd

# ----------- Create a mask from all segmentation files

# get all files
zone = 'AV'
in_fold = path.join('/DATA/CARHAB/00_Data/segmentation', zone)
pattern = path.join(in_fold, '*.shp')
list_seg = glob.glob(pattern)
pprint(list_seg)

# Concat them
a_seg = geopandas.read_file(list_seg[0])
concat = a_seg
a_2nd_seg = geopandas.read_file(list_seg[1])

for a_seg_file in list_seg[1:]:
    a_seg = geopandas.read_file(a_seg_file)
    concat = pd.concat((concat, a_seg))

# Dissolve them in one geometry
pprint(concat.columns)
concat['dissolve'] = 1  # create a field on which perform the dissolve
pprint(concat['dissolve'])
dissolve = concat.dissolve(by='dissolve')

# perform difference with area
area_file = ('/DATA/CARHAB/00_Data/zones/Zone_PNRAv.shp')
area = geopandas.read_file(area_file)
difference = geopandas.overlay(area, dissolve, how='difference')

# save the result
mask_file = path.join(in_fold, '{}_masque_IGN.shp'.format(zone))
difference.to_file(mask_file)


